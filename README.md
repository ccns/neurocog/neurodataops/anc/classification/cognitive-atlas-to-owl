# Convert Cognitive Atlas into OWL

Uses [Cognitive Atlas Python module](https://cogat-python.readthedocs.io/en/latest/index.html) that retrieves the data with Cognitive Atlas API.

[**Download the most recent OWL file**](https://gitlab.com/api/v4/projects/44969841/jobs/artifacts/main/raw/cogatlas.owl?job=generate-owl)

## Install dependencies

```bash
python3 -m venv .venv
source .venv/bin/activate
```

```bash
pip install -U -r requirements.txt
```

## Execute

For usage help execute:
```bash
python3 cogat2owl.py --help
```

The following command generates Cognitive Atlas OWL with concepts and tasks as classes.

```bash
python3 cogat2owl.py -c -t
```

The cognitiveatlas module produces much command line output. You may want to output it to a file.

```bash
python3 cogatlas.py  -c -t > cogatlas.log
```

from cognitiveatlas.api import get_concept
from cognitiveatlas.api import get_task
import xml.etree.ElementTree as ET
import sys
import argparse

# Returns a list of subclass ids for a given concept.
def get_subclasses(id):
    subclasses = []
    concept_details = get_concept(id = id).json
    rels = concept_details['relationships']
    if rels and len(rels) > 0:
        # print(rels)
        for r in rels:
            if r['relationship'] == 'KINDOF' and r['direction'] == 'child':
                subclasses.append(r['id'])
    return subclasses

# Returns all Cognitive Atlas concepts.
# Clean up the names.
def get_all_concepts():
    result = get_concept()
    concepts = {}
    chars_to_remove = {'(', ')', '&', '=', '+', '@', '|', '-', ',', ';', "'", '*', '/', ':', '.', '"'}
    for concept in result.json:
        name = concept['name'].replace(' ', '_').capitalize()
        name = ''.join(c for c in name if c not in chars_to_remove)
        id = concept['id']
        concepts[id] = name
    return concepts

# Returns all Cognitive Atlas tasks.
# Clean up the names.
def get_all_tasks():
    result = get_task()
    tasks = {}
    chars_to_remove = {'(', ')', '&', '=', '+', '@', '|', '-', ',', ';', "'", '*', '/', ':', '.', '"'}
    for task in result.json:
        name = task['name'] \
            .replace(' ', '_') \
            .replace("&amp;", "and") \
            .capitalize()
        name = ''.join(c for c in name if c.isascii() and c not in chars_to_remove)
        id = task['id']
        if len(name) == 0:
            print("INFO: skipping task " + id + " with empty name")
            continue
        if not name.isascii():
            print("INFO: task " + id + " name not ascii")
        tasks[id] = name
    return tasks

# Returns subclass hierarchy for given concepts.
# `concepts` is a dict with id as key and name as value.
def get_concept_subclass_hierarchy(concepts):
    subclass_rels = []
    for concept_id in concepts.keys():
        subclasses = get_subclasses(concept_id)
        if subclasses:
            subclass_rels.append((concept_id, subclasses))
    return subclass_rels

def main(argv):
    parser = argparse.ArgumentParser(
        description = "Generates Cognitive Atlas OWL file by collecting \
            concepts and tasks using cognitiveatlas Python package.")
    c_group = parser.add_mutually_exclusive_group()
    c_group.add_argument(
        '--concept-classes', '-c',
        dest = "generate_concept_classes",
        action = "store_true",
        help = "If set, the concepts will be generated as classes."
    )
    c_group.add_argument(
        '--concept-classes-hierarchy', '-s',
        dest = "generate_concept_classes_hierarchy",
        action = "store_true",
        help = "If set, the concepts will be generated as classes retaining their subclass hierarchy."
    )
    c_group.add_argument(
        '--concept-individuals', '-i',
        dest = "generate_concept_individuals",
        action = "store_true",
        help = "If set, the concepts will be generated as individuals."
    )
    t_group = parser.add_mutually_exclusive_group()
    t_group.add_argument(
        '--tasks-classes', '-t',
        dest = "generate_task_classes",
        action = "store_true",
        help = "If set, the tasks will be generated as classes."
    )
    t_group.add_argument(
        '--task-individuals', '-j',
        dest = "generate_task_individuals",
        action = "store_true",
        help = "If set, the tasks will be generated as individuals."
    )
    args = parser.parse_args()

    concepts = {}
    subclass_rels = []
    tasks = {}
    
    if args.generate_concept_classes or args.generate_concept_classes_hierarchy or args.generate_concept_individuals:
        concepts = get_all_concepts()
        if args.generate_concept_classes_hierarchy:
            subclass_rels = get_concept_subclass_hierarchy(concepts)
    
    if args.generate_task_classes or args.generate_task_individuals:
        tasks = get_all_tasks()

    # Create ontology node
    root = ET.Element('Ontology')

    # Add concepts if requested
    if args.generate_concept_classes or args.generate_concept_classes_hierarchy or args.generate_concept_individuals:
        mental_concept_class_name = 'Linguistic_Term'
        decl_mental_concept = ET.SubElement(root, 'Declaration')
        decl_class = ET.SubElement(decl_mental_concept, 'Class')
        decl_class.attrib['IRI'] = '#' + mental_concept_class_name
        concepts = get_all_concepts()

        # Generate concept classes
        if args.generate_concept_classes or args.generate_concept_classes_hierarchy:
            for concept_name in concepts.values():
                decl = ET.SubElement(root, 'Declaration')
                decl_class = ET.SubElement(decl, 'Class')
                decl_class.attrib['IRI'] = '#' + concept_name
                # Subclass of Mental_concept
                decl = ET.SubElement(root, 'SubClassOf')
                # Child
                decl_class = ET.SubElement(decl, 'Class')
                decl_class.attrib['IRI'] = '#' + concept_name
                # Parent
                decl_class = ET.SubElement(decl, 'Class')
                decl_class.attrib['IRI'] = '#' + mental_concept_class_name
            # Generate concept classes hierarchy
            if args.generate_concept_classes_hierarchy:
                subclass_rels = get_concept_subclass_hierarchy(concepts)
                # Generate mental concept subclass relationships
                for rel in subclass_rels:
                    for child in rel[1]:
                        if child in concepts:
                            decl = ET.SubElement(root, 'SubClassOf')
                            # Child
                            decl_class = ET.SubElement(decl, 'Class')
                            decl_class.attrib['IRI'] = '#' + concepts[child]
                            # Parent
                            decl_class = ET.SubElement(decl, 'Class')
                            decl_class.attrib['IRI'] = '#' + concepts[rel[0]]
                        else:
                            print("ERROR: concept " + child + " not in concepts")

        # Generate concept individuals
        if args.generate_concept_individuals:
            for concept_name in concepts.values():
                decl = ET.SubElement(root, 'Declaration')
                decl_class = ET.SubElement(decl, 'Individual')
                decl_class.attrib['IRI'] = '#' + concept_name
                # Individual of Mental_concept
                decl = ET.SubElement(root, 'ClassAssertion')
                # Class
                decl_class = ET.SubElement(decl, 'Class')
                decl_class.attrib['IRI'] = '#' + mental_concept_class_name
                # Individual
                decl_class = ET.SubElement(decl, 'Individual')
                decl_class.attrib['IRI'] = '#' + concept_name

    # Add tasks if requested
    if args.generate_task_classes or args.generate_task_individuals:
        task_class_name = 'Cognitive_Task'
        decl_task = ET.SubElement(root, 'Declaration')
        decl_class = ET.SubElement(decl_task, 'Class')
        decl_class.attrib['IRI'] = '#' + task_class_name
        tasks = get_all_tasks()

        # Generate task classes
        if args.generate_task_classes:
            for task_name in tasks.values():
                decl = ET.SubElement(root, 'Declaration')
                decl_class = ET.SubElement(decl, 'Class')
                decl_class.attrib['IRI'] = '#' + task_name
                # Subclass of Task
                decl = ET.SubElement(root, 'SubClassOf')
                # Child
                decl_class = ET.SubElement(decl, 'Class')
                decl_class.attrib['IRI'] = '#' + task_name
                # Parent
                decl_class = ET.SubElement(decl, 'Class')
                decl_class.attrib['IRI'] = '#' + task_class_name

        # Generate task individuals
        if args.generate_task_individuals:
            for task_name in tasks.values():
                decl = ET.SubElement(root, 'Declaration')
                decl_class = ET.SubElement(decl, 'Individual')
                decl_class.attrib['IRI'] = '#' + task_name
                # Individual of Task
                decl = ET.SubElement(root, 'ClassAssertion')
                # Class
                decl_class = ET.SubElement(decl, 'Class')
                decl_class.attrib['IRI'] = '#' + task_class_name
                # Individual
                decl_class = ET.SubElement(decl, 'Individual')
                decl_class.attrib['IRI'] = '#' + task_name

    # Write the ontology to file
    tree = ET.ElementTree(root)
    ET.indent(tree, space="\t")
    with open('cogatlas.owl', "w") as cogat_file:
        tree.write(cogat_file, encoding="unicode", xml_declaration=True)

if __name__ == '__main__':
    main(sys.argv)

